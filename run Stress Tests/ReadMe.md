This is help documentation on how to run Stress Tests

---By Nathan Hoisington


## INITIAL SETUP ##

	1. Appguard must be installed on the machine
	2. Install latest python2.7 (latest tested version was 2.7.9). (lower protection on AppGuard to allow)
	3. Install the latest Windows extentions for pyhon2.7 (latest tested was pywin32-219)
	Do one of the following
	4. place entire directory in Desktop(or easy to access place). Then exclude entire folder from AppGuard UserSpace policies
	--OR--
	4. place entire directory in a non-userspace dir (like c:\) and create a shortcuts for ease of use.
	5. (WARNING)DO NOT remove any of the files from their place in "run Stress Tests"
	6. do a test run.  run batch script cleanReg.bat...  you may get a permissions error.  if that is the case, then go into the APGStressTester and click edit with idle on "APGStressTest.py" and hit f5 to run.  This will import libraries for first time and script should run after that.

## Test Run##

	1. ungaurd "c:\Python27\python.exe" (or relative python path)
	2. run/click batch script "cleanReg.bat"
	3. Add "c:\Python27\python.exe" to guarded application with privacy mode ON, and MemoryWrite/MemoryRead ON.  
	4. run/click a batch script accordingly "WriteTest.bat", "ReadTest.bat", "RegistryTest.bat", "MemoryTest.bat" 


## Modify for preferences ##

###To modify app python tried to read memory from###

	1. add/remove application to TestProcesses list in "APGStressTester\MemoryTestBaseTesting.py"	
	2. modify "MemoryTest.bat" (right click edit).  now set to start notepad.exe before test and kill it after. just change notepad.exe to something else, or add lines to start more processes.

###To modify how many times a test is run.###

	1. modify some "*Test.bat" file and change line "times=100" to a number of your choosing.


### Verify Resulst
	
	* all results for the test run will be in the run Stress Tests\Mytestlog.txt file
	* 0 return means blocked, 1 return means allowed
