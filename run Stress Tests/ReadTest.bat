@echo off       
set times=1000000

::CHecking if WINDOWS is XP
ver | find "5." > nul

::location of MyPrivateFolder for xp
if %ERRORLEVEL% == 0 (
   echo xp
   echo testing > %userprofile%\MyDocuments\MyPrivateFolder\test.txt   
)

::location of MyPrivateFolder for OTHER WINDOWS
if NOT %ERRORLEVEL% == 0 (
   echo not xp
   echo testing > %userprofile%\Documents\MyPrivateFolder\test.txt
)

echo running read test
c:\Python27\python.exe RunTest.py -r %times%


