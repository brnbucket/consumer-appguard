from APGStressTester.APGStressTest import *
import sys, getopt


def main(argv):

    try:
        opts, args = getopt.getopt(argv,"cr:w:m:R:", ['read=','write=','memory=','registry='])
    
    except getopt.GetoptError:
        print 'failed to run error'

        
    for opt, arg in opts:
        if opt == '-c':
            RunBeforeTest()
        if  opt == '-r':
            try:
                times = int(arg.strip('/n'))
            except:
                times = 100
            ReadTestLoop(times)
        if opt == '-w':
            try:
                times = int(arg.strip('/n'))
            except:
                times = 100
            WriteTestLoop(times)
        if opt == '-R':
            try:
                times = int(arg.strip('/n'))
            except:
                times = 100
            RegistryTestLoop(times)
        if opt == '-m':
            try:
                times = int(arg.strip('/n'))
            except:
                times = 100
            MemoryTestLoop(times)
if __name__=="__main__":
    main(sys.argv[1:])
        


