'''
This is for testing appguard v 3.0.5 expected protected registries:
"HKEY_LOCAL_MACHINE"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\RunEx"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\Explorer\\Run"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows NT\CurrentVersion\\Terminal Server\\Install"
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders" 
"HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders" 
"HKEY_CURRENT_USER\\Software\\Microsoft\Windows NT\CurrentVersion\\Windows" 
"HKEY_CURRENT_USER\\Software\\Classes" 
"HKEY_CURRENT_USER\\Control Panel\Desktop" 
"HKEY_USERS\\.DEFAULT" 
"HKEY_USERS\\S-1-5-18"


Usage:

regtest()tests these registry functions:
CreateKey
CreateKeyEx
SetValue
SetValueEx
DeleteKey
DeleteKeyEx
DeleteValue
regtest(pretest ='yes') should be run prior to guarding python


a log will be created as a result of this function in my documents stating what
keys it wrote to
In otherwords, appguard should block writes to HKLM and allow writes to HKCU

Dependancies:
user interaction for adding python to list 
Appguard
must have python 2.6+ installed on computer
'''



from _winreg import *
import os
import platform
from time import sleep, asctime
from ctypes import *
import re

global system
system = platform.win32_ver()[0]
global mydoc
mydoc = os.environ['userprofile'] + '\\My Documents'

global userxml
if '7' in system or 'VISTA' in system or 'Vista' in system:
    userxml = os.environ['userprofile'] + '\\AppData\\Roaming\\Blue Ridge Networks\\Appguard\\AppGuardPolicy.xml'
else:
    userxml = os.environ['userprofile'] + '\\Application Data\\Blue Ridge Networks\\AppGuard\\AppGuardPolicy.xml'


messagebox = windll.user32.MessageBoxA
MB_OKCANCEL = 0x00000001L
            

def connectreg(Hive):
    if Hive == 'HKEY_CURRENT_USER':
        reg = ConnectRegistry(None,HKEY_CURRENT_USER)
    elif Hive == 'HKEY_LOCAL_MACHINE':
        reg = ConnectRegistry(None,HKEY_LOCAL_MACHINE)
    elif Hive == 'HKEY_CLASSES_ROOT':
        reg = ConnectRegistry(None,HKEY_CURRENT_CONFIG)
    elif Hive == 'HKEY_CLASSES_ROOT':
        reg = ConnectRegistry(None,HKEY_CLASSES_ROOT)
    elif Hive == 'HKEY_USERS':
        reg = ConnectRegistry(None,HKEY_USERS)
    return reg

 
def listsubkeys(Hive = 'HKEY_CURRENT_USER', Key = "testing"):
    
    subkey = []   

    reg = connectreg(Hive)
    try:
        key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
    except:
        key =''
    if key:
        try:
            i = 0
            while 1:
                skey = EnumKey(key, i)
                subkey.append(skey)
                i += 1
        except:
            pass
    
        CloseKey(key)
    CloseKey(reg)
    return subkey

global HKLMKEYS
HKLMkeys = listsubkeys(Hive = 'HKEY_LOCAL_MACHINE', Key = '')
    
global HKCUkeys
HKCUkeys = ("Software\Microsoft\Windows\CurrentVersion\Run",
            "Software\Microsoft\Windows\CurrentVersion\RunOnceEx",
            "Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run",
            "Software\Microsoft\Windows NT\CurrentVersion\Terminal Server\Install",
            "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders",
            "Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders",
            "Software\Microsoft\Windows NT\CurrentVersion\Windows",
            "Software\Classes",
            "Control Panel\Desktop")
global HKUkeys
HKUkeys = ('.DEFAULT',
            'S-1-5-18')
    
def clean(Hive, Key):
    reg = connectreg(Hive)
    try:
        key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
    except:
        key =''
        pass
    try:
        if key:
            CloseKey(key)
            subkeys = listsubkeys(Hive = Hive, Key = Key)
            #find subkeys of subkeys
            subkeys2 =[]
            for i in subkeys:
                skeys2 = listsubkeys(Hive = Hive, Key = Key + '\\' + i)
                if skeys2:
                    for x in skeys2:
                        if 'CreateKey' in x or 'SetValue' in x:
                            key = OpenKey(reg, Key + '\\' + i)
                            DeleteKey(key, x)
                            CloseKey(key)
                else:
                    if 'CreateKey' in i or 'SetValue' in i:
                        key = OpenKey(reg, Key)
                        DeleteKey(key, i)
                        CloseKey(key)
    except:
        pass
    CloseKey(reg)
def cleanup():
     
        #write to HKEY_CURRENT_USER keys
    for w in range(0,4):
        for x in HKCUkeys:
            clean('HKEY_CURRENT_USER',x)
        #write to HKEY_LOCAL_MACHINE keys
        for y in HKLMkeys:
            clean('HKEY_LOCAL_MACHINE',y)
        #write to HKEY_USERS keys
        for z in HKUkeys:
            clean('HKEY_USERS',z)
                     


def log(message, log = 'reg.log',mode = 'a'):
    print asctime(), ': ', message
    try:
        event = open(log, mode)
        event.write(message + '\n')
        event.close()
    except:
        print 'could not log event, python is probably a guarded app'


def regtest(Hive= 'HKEY_CURRENT_USER', Key = "testing", pretest = 'no'):
    reg = connectreg(Hive)
    try:
        key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
    except:
        key =''
        pass
    result = 0
    if key:
        CloseKey(key) 
        if pretest == 'yes':
            try:
                key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
                CreateKey(key, 'CreateKey')
                CloseKey(key)
                RegCreateKey = 1
            except:
                RegCreateKey = 0
                CloseKey(key)
            try:
                key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
                CreateKeyEx(key, 'CreateKeyEx')
                CloseKey(key)
                RegCreateKeyEx = 1
            except:
                RegCreateKeyEx = 0
                CloseKey(key)
        
            try:
                key = OpenKey(reg, Key + '\\CreateKey', 0, KEY_ALL_ACCESS)
                SetValue(key, 'SetValue', REG_SZ, 'SetValue')
                CloseKey(key)
                RegSetValue= 1
            except:
                RegSetValue = 0
                CloseKey(key)

            try:
                key = OpenKey(reg, Key + '\\CreateKeyEx', 0, KEY_ALL_ACCESS)
                SetValueEx(key, 'CreateKeyEx', 0, REG_SZ, 'CreateKeyEx')
                CloseKey(key)
                RegSetValueEx= 1
            except:
                RegSetValueEx = 0
                CloseKey(key)
        else:
            try:
                key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
                CreateKey(key, 'CreateKey2')
                CloseKey(key)
                RegCreateKey = 1
            except:
                RegCreateKey = 0
                CloseKey(key)
            
            try:
                key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
                CreateKeyEx(key, 'CreateKeyEx2')
                CloseKey(key)
                RegCreateKeyEx = 1
            except:
                RegCreateKeyEx = 0
                CloseKey(key)
        
            try:
                key = OpenKey(reg, Key + '\\CreateKey', 0, KEY_ALL_ACCESS)
                SetValue(key, 'SetValue', REG_SZ, 'seting value')
                CloseKey(key)
                key = OpenKey(reg, Key + '\\CreateKey\\SetValue', 0, KEY_ALL_ACCESS)
                if EnumValue(key, 0)[1].encode('latin - 1', 'ignore') == 'seting value':
                    RegSetValue= 1
                else:
                    RegSetValue = 0
            except:
                RegSetValue = 0
                CloseKey(key)
            try:
                key = OpenKey(reg, Key +  '\\CreateKeyEx', 0, KEY_ALL_ACCESS)
                SetValueEx(key, 'CreateKeyEx', 0, REG_SZ, 'setting value ex')
    
                if EnumValue(key, 0)[1].encode('latin - 1', 'ignore') == 'setting value ex':
                    RegSetValueEx= 1
                else:
                    RegSetValueEx = 0
                CloseKey(key)
            except:
                RegSetValueEx = 0
                CloseKey(key)
     
            try:
                key = OpenKey(reg, Key + '\\CreateKeyEX', 0, KEY_ALL_ACCESS)
                try:
                    DeleteValue(key, 'SetValueEx')
                except:
                    DeleteValue(key, 'CreateKeyEX')
                CloseKey(key)
                RegDeleteValue= 1
            except:
                RegDeleteValue = 0
                CloseKey(key)

            try:
                key = OpenKey(reg, Key+ '\\CreateKey', 0, KEY_ALL_ACCESS)
                DeleteKey(key, 'SetValue')
                CloseKey(key)
                RegDeleteKey= 1
            except:
                RegDeleteKey = 0
                CloseKey(key)
            
            
            ###this function is not supported on this platform
            if system == 'XP':
                pass
            else:
                try:
                    key = OpenKey(reg, Key, 0, KEY_ALL_ACCESS)
                    DeleteKeyEx(key, 'CreateKeyEx')
                    CloseKey(key)
                    RegDeleteKeyEx= 1
                except:
                    RegDeleteKeyEx = 0
                    CloseKey(key)

        CloseKey(reg)
        log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + ' RegCreateKey = ' + str(RegCreateKey))
        log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + ' RegCreateKeyEx = ' + str(RegCreateKeyEx))
        log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + ' RegSetValue = ' + str(RegSetValue))
        log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + ' RegSetValueEx = ' + str(RegSetValueEx))
        try:
            log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + 'RegDeleteValue = ' + str(RegDeleteValue))
        except:
            pass
        try:
            log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + 'RegDeleteKey = ' + str(RegDeleteKey))
        except:
            pass
        try:
            log(str(re.findall(r'..:..:..',asctime())[0])+ ' : ' + Hive + '\\' + Key + 'RegDeleteKeyEx = ' + str(RegDeleteKeyEx))
        except:
            pass

        
    try:
        if RegCreateKey + RegCreateKeyEx +RegSetValue+ RegSetValueEx > 0:
            result = 1
        else:
            result = 0
    except:
        retult = 0

    return result
def registrytest(pretest = 'no', clean = 'no'):
    log('registry test ==> 0 = denied action, 1 = allowed action', mode = 'w')
    
    if clean == 'yes': 
        cleanup()    
    else:
        if pretest == 'yes':
            cleanup()
            #write to HKEY_CURRENT_USER keys
            if len(HKCUkeys) > 0:
                for i in HKCUkeys:
                    result1 = regtest(Hive= 'HKEY_CURRENT_USER', Key = i, pretest = 'yes')
            else:
                retult1 = 0

            #write to HKEY_LOCAL_MACHINE keys
            if len(HKLMkeys) > 0:
                for i in HKLMkeys:
                    result2 = regtest(Hive= 'HKEY_LOCAL_MACHINE', Key = i, pretest = 'yes')
            else:
                result2 = 0

            #write to HKEY_USERS keys
            if len(HKUkeys) > 0:
                for i in HKUkeys:
                    result3 = regtest(Hive= 'HKEY_USERS', Key = i, pretest = 'yes')
            else:
                result3 = 0
        else:
            #write to HKEY_CURRENT_USER keys
            if len(HKCUkeys) > 0:
                for i in HKCUkeys:
                    result1 = regtest(Hive= 'HKEY_CURRENT_USER', Key = i)
            else:
                result1 = 0
                
            #write to HKEY_LOCAL_MACHINE keys
            if len(HKLMkeys) > 0:
                
                for i in HKLMkeys:
                    result2 = regtest(Hive= 'HKEY_LOCAL_MACHINE', Key = i)
            else:
                result2 = 0
            #write to HKEY_USERS keys
            if len(HKUkeys) > 0:  
                for i in HKUkeys:
                    result3 = regtest(Hive= 'HKEY_USERS', Key = i)
            else:
                result3 = 0
    result = result1 + result2 + result3 
    return result


    

