'''
To run:
put python as Guarded application with mem and privacy protection and exicute
add text.txt to MyPrivateFolder
must prepare registry (run registry test prepare when not protected ==>
cleanup()
registrytest(pretest = 'yes')

'''


from MemoryTestBaseTesting import *
from regtest import *
from dlltest import *
import thread


MyPrivateFolder = os.environ['USERPROFILE'] + '\\Documents\\MyPrivateFolder'

def RunBeforeTest(): #must be run when python is not guarded
    cleanup()
    registrytest(pretest = 'yes')


def WriteTestLoop(times): 
    result = 0
    t = 0
    timesblocked = 0
    timesnotblocked = 0
    log('************testing Write*************', log= 'writelog.log', mode = 'a')
    while (t < times):
        if os.path.exists('C:\\test'):
            pass
        else:
            os.mkdir('C:\\test')
        try:
            f = open('C:\\test\\test.txt', 'w')
            f.write('testing')
            result = 1
            timesnotblocked +=1
            log ('ABLE TO WRITE', log='writelog.log')
            f.close()
            #os.remove('C:\\test\\test.txt')
            #break
        except:
            result = 0
            timesblocked += 1
            log ( 'unable to write', log='writelog.log')
        t+=1
    Log('times blocked = %i, times not blocked = %i', log='writelog.log')
    if result == 1:
        print ('test failed')
        
def ReadTestLoop(times):
    result = 0
    t = 0
    timesblocked = 0
    timesnotblocked = 0
    log('***************testing read***************', log= 'readlog.log', mode = 'a')
    while (t < times):
        try:
            f = open(MyPrivateFolder + '\\test.txt', 'r')
            result = 1
            log ('ABLE TO read', log='readlog.log')
            timesnotblocked +=1
            f.close()
            #break
        except:
            result = 0
            timesblocked +=1
            log ( 'unable to read', log='readlog.log')
        t+=1
    Log('times blocked = %i, times not blocked = %i', log='readlog.log')
    if result == 1:
        print ('test failed see readlog.log')
       
def RegistryTestLoop(times):
    result = 0
    t = 0
    while result == 0 and (t < times):
        
        print (asctime(), 'running registry test')
        result = registrytest()
        t+=1
    if result == 1:
        print ('failed registry test')
        #MessageBox(None, 'should never be able to access registry but it did', 'Registry Test Failure', 0x000000001L)
        log ('registry access blocked', log='reg.log')
    else:
        log ('registry access not blocked', log='reg.log')
def MemoryTestLoop(times):
    result = 0
    t = 0
    while result == 0 and (t < times):
        print ('=======================================')
        print (asctime(), 'running memory test')
        print ('=======================================')
        results = test(logevents = False)
        sleep(LoopRestTime)
        result = 0
        for i in results:
            read = int(i[0])
            write = int(i[1])
            if read == 1:
                print (asctime(), 'memory read allowed')
            if write == 1:
                print (asctime(), 'memory write allowed')
            result += read
            result += write
        t+=1
    if result == 1:        
        print ('failed memory test')
        MessageBox(None, 'should never be able to access memory but it did', 'Memory Test Failure', 0x000000001L)



#if __name__ != "__main__":
    #print ('loop test')
    #thread.start_new_thread(WriteTestLoop,()) #Guarded Execution protection
    #thread.start_new_thread(ReadTestLoop,()) #privacy protection
    #thread.start_new_thread(MemoryTestLoop,()) #Memory protection
    #thread.start_new_thread(RegistryTestLoop,()) #registry protection

