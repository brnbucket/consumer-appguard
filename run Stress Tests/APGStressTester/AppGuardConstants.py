
'''
this file should not be in a userspace folder
'''
import os
from ctypes import *
from ctypes.wintypes import *
import platform
from win32com.client import Dispatch
import struct
from time import sleep, asctime
IEwaittimeout = 120


MessageBox = windll.user32.MessageBoxA
MB_OKCANCEL = 0x1l
FindWindow = windll.user32.FindWindowA
FindWindowEx = windll.user32.FindWindowExA
GetWindow = windll.user32.GetWindow
GetMenu = windll.user32.GetMenu
GetMenuItemCount = windll.user32.GetMenuItemCount
GetMenuInfo = windll.user32.GetMenuInfo
GetSubMenu = windll.user32.GetSubMenu
SendMessage = windll.user32.SendMessageA
PostMessage = windll.user32.PostMessageA
GetMenuItemID = windll.user32.GetMenuItemID
GetWindowText = windll.user32.GetWindowTextA
SendMessage = windll.user32.SendMessageA
SetParent = windll.user32.SetParent
DestroyWindow = windll.user32.DestroyWindow
TRACKBAR_CLASSA = 'msctls_trackbar32'
TBM_SETPOS = 1029
GetWindowThreadProcessId = windll.user32.GetWindowThreadProcessId
VirtualAllocEx = windll.kernel32.VirtualAllocEx
VirtualQueryEx = windll.kernel32.VirtualQueryEx
OpenProcess = windll.kernel32.OpenProcess
ReadProcessMemory = windll.kernel32.ReadProcessMemory
WriteProcessMemory = windll.kernel32.WriteProcessMemory
VirtualFreeEx = windll.kernel32.VirtualFreeEx
GetSystemInfo = windll.kernel32.GetSystemInfo
CloseHandle = windll.kernel32.CloseHandle
FlushInstructionCache = windll.kernel32.FlushInstructionCache
GetLastError = windll.kernel32.GetLastError

WM_CLOSE = 0x0010
BM_CLICK = 245
WM_RBUTTONDOWN = 0x204
WM_USER = 1024
TBM_GETPOS = (WM_USER)
GW_CHILD = 5
GW_HWNDNEXT = 2
WM_SETTEXT = 12
WM_SETFOCUS = 0x0007

TB_GETBUTTON = (WM_USER + 23)
TB_BUTTONCOUNT = (WM_USER + 24)
TB_GETITEMRECT = (WM_USER + 29)
TB_GETBUTTONTEXT = (WM_USER + 45)
LVM_FIRST = 4096 
LVM_GETITEMTEXTA = (LVM_FIRST + 45)
LVM_GETITEMCOUNT = (LVM_FIRST + 4)
LVM_GETITEMA = (LVM_FIRST + 5)
MEM_RESERVE = 8192
MEM_COMMIT = 4096
PAGE_READWRITE = 4
WM_LBUTTONDOWN = 513
WM_LBUTTONUP = 514
WM_KEYUP = 0x0101
WM_COMMAND = 273
LVM_SETITEMSTATE = (LVM_FIRST + 43)
LVM_GETITEMSTATE = (LVM_FIRST + 44)
LVIF_COLFMT = 0x00010000
LVIF_COLUMNS = 0x0200
PAGE_READONLY = 0x02
PROCESS_ALL_ACCESS = 0x1F0FFF

MEM_RESERVE = 8192
MEM_COMMIT = 4096
PAGE_READWRITE = 4
WM_LBUTTONDOWN = 513
WM_LBUTTONUP = 514
PROCESS_ALL_ACCESS = 0x1F0FFF
MEM_RELEASE = 32768
WM_GETTEXT = 0x000D
WM_KEYDOWN = 0x0100
VK_RBUTTON = 0x01
VK_LBUTTON = 0x02
VK_UP = 0x26
VK_RETURN = 0x0D
VK_RIGHT = 0x27
VK_DOWN = 0x28


CreateProcess = windll.kernel32.CreateProcessA
NORMAL_PRIORITY_CLASS = 0x20
CREATE_PRESERVE_CODE_AUTHZ_LEVEL =  0x02000000

### folders and locations
try:
     programfiles = os.environ['PROGRAMFILES(X86)']
except:
     programfiles = os.environ['programfiles']
     
brnpolicylocation = programfiles + "\\Blue Ridge Networks\\AppGuard Enterprise\\brnpolicy.xml"
userspace = os.environ['userprofile']
mydoc = userspace + '\\My Documents'
if os.path.exists(mydoc):
     pass
else:
     mydoc = userspace + '\\Documents'

sysroot = os.environ['systemroot']
pwd = os.getcwd()
note = sysroot + '\\system32\\notepad.exe'
#privatefolder = userspace + '\\My Documents\\Private Folder'
osys = platform.platform()
if '7' in osys or 'VISTA' in osys or 'Vista' in osys:
    userxml = userspace + '\\AppData\\Roaming\\Blue Ridge Networks\\Appguard\\AppGuardPolicy.xml'
else:
    userxml = userspace + '\\Application Data\\Blue Ridge Networks\\AppGuard\\AppGuardPolicy.xml'

###structures


class MEMORY_BASIC_INFORMATION(Structure):
     _fields_ =[("BaseAddress", c_void_p),#i think
                ("AllocationBase", c_void_p), #i think
                ("AllocationProtect", c_ulong), 
                ("RegionSize",c_size_t), #i think
                ("State", c_ulong),
                ("Protect", c_ulong),
                ("Type", c_ulong)]

                
class SYSTEM_INFO(Structure):
     _fields_ = [("wProcessorArchitecture", c_ushort),
                ("wReserved", c_ushort),
                ("dwPageSize", c_ulong),
                ("lpMinimumApplicationAddress", c_void_p),
                ("lpMaximumApplicationAddress", c_void_p),
                ("dwActiveProcessorMask", c_ulong), #i think
                ("dwNumberOfProcessors", c_ulong), 
                ("dwProcessorType", c_ulong),
                ("dwAllocationGranularity", c_ulong),
                ("wProcessorLevel", c_ushort),
                ("wProcessorRevision", c_ushort)]





class STARTUPINFO(Structure):
    _fields_ = [("cb", c_ulong),
                ("lpReserved", c_char_p), #i think
                ("lpDesktop", c_char_p), #i think
                ("lpTitle", c_char_p), #i think
                ("dwx", c_ulong),
                ("dwy", c_ulong),
                ("dwXSize", c_ulong),
                ("dwYSize", c_ulong),
                ("dwXCountChars", c_ulong),
                ("dwYCountChars", c_ulong),
                ("dwFillAttribute", c_ulong),
                ("dwFlags", c_ulong),
                ("wShowWindow", c_ushort),
                ("cbReserved2", c_ushort),
                ("lpReserved2", POINTER(c_byte)),#?
                ("hStdInput", c_void_p), 
                ("hStdOutput", c_void_p),
                ("hStdError", c_void_p)]
    
class PROCESSINFO(Structure):
    _fields_ = [("hProcess", c_void_p),
                ("hThread", c_void_p), 
                ("dwProcessId", c_ulong), 
                ("dwThreadId", c_ulong)]

class STARTUPINFO(Structure):
    _fields_ = [("cb", c_ulong),
                ("lpReserved", c_char_p), #i think
                ("lpDesktop", c_char_p), #i think
                ("lpTitle", c_char_p), #i think
                ("dwx", c_ulong),
                ("dwy", c_ulong),
                ("dwXSize", c_ulong),
                ("dwYSize", c_ulong),
                ("dwXCountChars", c_ulong),
                ("dwYCountChars", c_ulong),
                ("dwFillAttribute", c_ulong),
                ("dwFlags", c_ulong),
                ("wShowWindow", c_ushort),
                ("cbReserved2", c_ushort),
                ("lpReserved2", POINTER(c_byte)),#?
                ("hStdInput", c_void_p), 
                ("hStdOutput", c_void_p),
                ("hStdError", c_void_p)]


class RECT(Structure):
    _fields_ = [("left", c_long),
                ("top", c_long), 
                ("right", c_long), 
                ("bottom", c_long)]

class TBBUTTON(Structure):
    _fields_ = [("iBitmap", c_int),
                ("idCommand", c_int), #maybe
                ("fsState", c_byte), #maybe
                ("fsStyle", c_byte),
                #("bReserved[6]", c_byte),64bit
                ("bReserved[2]", c_byte),
                ("dwData", c_int),
                ("iString", c_long)]

class LVITEM(Structure):
    _fields_ = [("mask", c_uint),
                ("iItem", c_int),
                ("iSubItem", c_int), 
                ("state", c_uint),
                ("stateMask", c_uint),
                ("pszText", c_char_p),##??
                ("cchTextMax", c_int),
                ("iImage", c_int),
                ("lParam", c_long),##??
                ("iIndent", c_int),#if (_WIN32_IE >= 0x0300)
                ("iGroupId", c_int), #if (_WIN32_WINNT >= 0x0501)
                ("cColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("puColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("piColFmt", c_int), #if (_WIN32_WINNT >= 0x0600)
                ("iGroup", c_int)] #if (_WIN32_WINNT >= 0x0600)

#functions

def ClickWindow(windowhandle, xcor,ycor):
    '''clicks window'''
    clickcoor = (ycor << 16) | xcor
    SendMessage(windowhandle,WM_LBUTTONDOWN,0,clickcoor)
    SendMessage(windowhandle,WM_LBUTTONUP,0,clickcoor)

def ClickButton(window,text):
    '''clicks button in window'''
    Button = FindWindowEx(window, None, 'Button', text)
    SendMessage(Button,BM_CLICK, True, 0)
def DBLClickButton(window,text):
    '''clicks button in window'''
    Button = FindWindowEx(window, None, 'Button', text)
    SendMessage(Button,BM_CLICK, True, 0)
    SendMessage(Button,BM_CLICK, True, 0)
    
def kill(pid):
    """kill function for Win32"""
    handle = OpenProcess(1, 0, pid)
    print 'killing ', pid
    return (0 != windll.kernel32.TerminateProcess(handle, 0)) 

def findpid(CLASS, WINDOW):
    '''finds pid of given window''' 
    windowfind = FindWindow(CLASS, WINDOW)
    pid = create_string_buffer(4)
    p_pid = addressof(pid)
    GetWindowThreadProcessId(windowfind, p_pid)
    return struct.unpack("i",pid)[0]

def getrunningprocesses():
    '''gets all running processes''' 
    from win32com.client import GetObject
    WMI = GetObject('winmgmts:')
    processes = WMI.InstancesOf('Win32_Process')
    pros = [process.Properties_('Name').Value.encode('latin -1', 'ignore') for process in processes] # get the process names
    pids = [process.Properties_('ProcessID').Value for process in processes]

    processes = []
    for i in range(len(pros)):
        append = pros[i],pids[i]
        #print append
        processes.append(append)
    
    return processes
 
def StartProc(exe, params = 0):
     '''starts processes'''
     startinfo = STARTUPINFO()
     procinfo = PROCESSINFO()
     startinfo.cb = sizeof(startinfo)
     attemptstostart = 10
     while attemptstostart > 0:
          try:
               CreateProcess(exe,
                             params,
                             0,
                             0,
                             True,
                             0x00000080,# NORMAL_PRIORITY_CLASS, #CREATE_PRESERVE_CODE_AUTHZ_LEVEL,
                             0,
                             0,
                             addressof(startinfo),
                             addressof(procinfo))
               sleep(.1)
               break
          except WindowsError as error:
               if "access violation reading" in error:
                    #too soon to start process after starting/killing another
                    attemptstostart -= 1
                    sleep(1)
               else:
                    print "exception: ", error
                    return -1

     pid = procinfo.dwProcessId
     sleep(.1)

     CloseHandle(procinfo.hProcess)
     CloseHandle(procinfo.hThread)
     pid = procinfo.dwProcessId
     return pid


def log(message, log = 'Mytestlog.txt' ,mode = 'a'):
    '''
    message: message to log
    log: path to log file
    mode: type of write to log file
    if mode = 'a':appends to file
    if mode = 'w':writes new file
    '''
    print str(asctime()) + ': ' + message
    try:
        event = open(log, mode)
        event.write(str(asctime()) + ': ' + message + '\n')
        event.close()
    except:
        print 'could not log event'
  

