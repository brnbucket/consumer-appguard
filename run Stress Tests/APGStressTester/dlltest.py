from subprocess import Popen
'''
must run my testing process because of the window name
'''
from AppGuardConstants import *
from time import sleep

dlllocation = 'test.dll' #placed in local dir of dlltest.py
NumberofRuns = 50

def dllruntest(Times):
    for i in range(Times):
        Popen('rundll32.exe test.dll test',shell = True)
        
def CleanUp():
    failed = FindWindow(None, 'RunDLL')
    succeed = FindWindow(None, 'dll test')

    failedtimes = 0
    successtimes = 0
    while failed > 0:
        failedpid = findpid(None, 'RunDLL')
        kill(failedpid)
        failed = FindWindow(None, 'RunDLL')
        failedtimes +=1
        sleep(.1)
        

    while succeed > 0:
        successpid = findpid(None, 'dll test')
        kill(successpid)
        succeed = FindWindow(None, 'dll test')
        successtimes +=1
        sleep(.1)
        

    return failedtimes, successtimes

if __name__ == '__main__':
    dllruntest(NumberofRuns)
    f,s = CleanUp()
    print ('out of ' + str(NumberofRuns) + ' attempts at dll launch',
           str(f) + ' number of attempts were blocked. /n',
           str(s) + ' number of attempts were not blocked.')
    
