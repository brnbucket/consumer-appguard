import os
import sys

pathToModules = 'Modules'

if not os.path.exists(pathToModules):
    print('%s does not exist' % pathToModules)
else:
    sys.path.append(pathToModules)
    from Constants import *
    from Gui import *
    from MemoryTestBaseTesting import *
    from systr import *
    from XMLParser import *
    from MyThread import *
pathToTestProcesses = 'TestProcesses'

if not os.path.exists(pathToTestProcesses):
    print('%s does not exist' % pathToTestProcesses)


###Add Remove Python from Guarded applications

class AppGuardGui():
    def __init__(self):
        self.GetAppGuardGui()
        self.GetWindowHandles()
    def GetAppGuardGui(self):
        self.GuiLevel = FindWindow(None,'Blue Ridge AppGuard')
        self.GuiCustom = FindWindow(None,'AppGuard Customize')
    def GetWindowHandles(self):
        nRet = 0
        if nRet == 0:
            self.Trackbar = FindWindowEx(self.GuiLevel, None, TRACKBAR_CLASSA, None)
            self.systab = FindWindowEx(self.GuiCustom, None, 'SysTabControl32', None)
            self.ADVTab = GetWindow(self.systab,GW_CHILD)
            self.GATab = GetWindow(self.ADVTab,GW_HWNDNEXT)
            self.UserSpaceTab = GetWindow(self.GATab,GW_HWNDNEXT)
            self.AlertsTab = GetWindow(self.UserSpaceTab,GW_HWNDNEXT)
            self.GAListView = FindWindowEx(self.GATab, None, 'SysListView32', None)
    def DeleteGuardedApp(self, app):
        GetListView(self.GAListView, column_index = 0, FocusText = app)
        find = FindMyWindow()
        find.ListWindows(self.GuiCustom, ['Button', 'Delete', True])
        self.Apply()
    def AddGuardedApp(self,
                      app,
                      privacy = False, #now default value
                      memorywrite = True, #now default value
                      memoryread = True, #now default value
                      ):
        #click add/program button
        nRet = 0
        if nRet == 0:
            for i in range(3):
                Click = ClickAndWait(WindowHandle = self.GATab,
                                     WindowName = "Add a Program",
                                     ButtonName = 'Add Program...')
                if Click == 1:
                    break
            if Click == 0:
                nRet = 1
                print ('could not add a program')
                    
        if nRet == 0:
            BrowseWin = FindWindow(None, "Add a Program")
            for i in range(3):
                Click = ClickAndWait(WindowHandle = BrowseWin,
                                     WindowName = "Open",
                                     ButtonName = '&Browse...')
                if Click == 1:
                    break
            if Click == 0:
                nRet = 1
                print ('could not browse')

        if nRet == 0:
            OpenWin = FindWindow(None, "Open")
            sleep(.1)
            if OpenWin == 0:
                print ('could not find open window')
                nRet = 1
        if nRet == 0:
            ComboBoxEx32 = FindWindowEx(OpenWin, None, 'ComboBoxEx32', None)
            ComboBox = FindWindowEx(ComboBoxEx32, None, 'ComboBox', None)
            edit = FindWindowEx(ComboBox, None, 'Edit', None)
            if edit == 0:
                print ('could not find edit text')
                nRet = 1
        if nRet == 0:
            print ('now editing app path')
            SendMessage(edit, WM_SETTEXT, 0, app)
            print ('clicking open')
            DBLClickButton(OpenWin,'&Open')

        if nRet == 0:
            BrowseWin = FindWindow(None, "Add a Program")
            if BrowseWin == 0:
                print('could not find add a program window')
                nRet = 1
        if nRet == 0:
            #now select values
            if privacy == True:
                ClickButton(BrowseWin, "Enable Privacy Mode")
            if memorywrite == False:
                ClickButton(BrowseWin, "Enable Memory Write Protection")
            if memoryread == False:
                ClickButton(BrowseWin, "Enable Memory Read Protection")

            #now finish:
            ClickButton(BrowseWin,"OK")
        if nRet == 0:
            if FindWindow(None, "Add a Program") > 0:
                print ('click button did not close gui, click again')
                ClickButton(BrowseWin,"OK")
                if FindWindow(None, "Add a Program") > 0:
                    print ('failed to click button')
        self.Apply()
    def Apply(self):
        Click = MyThread(ClickButton, (self.GuiCustom,'Apply'))
        #DBLClickButton(self.GuiCustom,'Apply')
        Click.run(timeout = .1)
        self.CheckAppGuardWindow()

    def SetSecurityLevel(self, Level):
        SetTrack = MyThread(SetTrackBar,(self.Trackbar, Level)) #0 is lockdown
        SetTrack.run(timeout = .1)
        self.CheckAppGuardWindow()

    def CheckAppGuardWindow(self):
        ApgWindow = FindWindow(None, 'AppGuard')
        if ( ApgWindow != 0 ):
               ClickButton( ApgWindow, '&Yes')
        
def ClickAndWait(Class = None,
                 WindowHandle = None,
                 WindowName = None,
                 ButtonName = None,
                 TimeExpected = 100, #one second
                 ):
    if (FindWindow(Class, WindowName) == 0):
        print ('clicking button')
        Click = MyThread(ClickButton, (WindowHandle, ButtonName))
        Click.run(timeout = .1)
        nret = WaitForWin(Class, WindowName, TimeExpected)
        return nret
    else: #dont need to click because gui exists already
        return 1

def WaitForWin(Class = None,
               WindowName = None,
               TimeExpected = 100, #one second
               ):
    print ('waiting for window %s' %WindowName)
    if ( FindWindow(Class,WindowName) !=0 ):
        return 1
    else:
        i = 0
        while FindWindow(Class,WindowName) == 0 and i < TimeExpected:
            sleep(.01)
            i+=1
    if ( FindWindow(Class,WindowName) !=0 ):
        print ('%s found' %WindowName)
        return 1
    else:
        print ('%s not found' %WindowName)
        return 0



def CreateGuardedAppsTest(Number):
    from shutil import copyfile
    ###lets create a dir and put a bunch of files in it
    MyGuardedFodler = 'c:\\MyGUARDEDAPFOLDER'
    #os.mkdir(MyGuardedFodler)

    ### Now lets copy notepad many times to this folder
    FileToCopy = '%s\\%s' %(pathToTestProcesses,'notepad.exe')
    for i in range(Number):
        DestinationFile = '%s\\file%i.exe' %(MyGuardedFodler,i)
        print('copying %s as %s' %(FileToCopy, DestinationFile))
        copyfile(FileToCopy,DestinationFile)

    ### Now lets add all processes in my guarded folder
    self = AppGuardGui()
    for File in os.listdir(MyGuardedFodler):
        print ('adding %s\\%s to Guarded applications list' %(MyGuardedFodler,File))
        self.AddGuardedApp('%s\\%s' %(MyGuardedFodler,File), privacy = True)
        
        


    
#self = AppGuardGui()
#self.AddGuardedApp('c:\\python34\\pythonw.exe', privacy = True)
        
