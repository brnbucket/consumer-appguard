'''
TO RUN Must place on desktop or other userspace
designed for AppGuard Consumer 4.1 testing
python 3.4
Scripts must use nathan.dll and test.dll
Script must use testvbs or other vbs that will run and exit within a second
'''
from time import sleep, asctime
from subprocess import Popen
from AppGuardGui import *
from MemoryTestBaseTesting import *

pathToTestProcesses = 'TestProcesses'
pathToSignedDLL = pathToTestProcesses + '\\nathan.dll'
pathToUnSignedDLL = pathToTestProcesses + '\\test.dll'
pathToUnsignedLaunch = pathToTestProcesses + '\\notepad.exe'
pathToScriptLaunch = pathToTestProcesses + '\\testvbs.vbs'
MyPrivateFolder = os.environ['USERPROFILE'] + '\\Documents\\MyPrivateFolder'
myPrivacyLocation = MyPrivateFolder + '\\test.txt'
TestMemoryProc = "notepad.exe"
myDelay = 3


class AppGuardTest():
    def _init_(self):
        self.SignedDLLResult = None
        self.UnsignedDLLResult = None
        self.WriteResult = None
        self.ReadResult = None
        self.ScriptResult = None
        self.MemoryReadResult = None
        self.MemoryWriteResult = None
    def RunSignedDll(self):
        print ('running signed dll launch')
        Popen('rundll32.exe %s test' %pathToSignedDLL,shell = True)
        sleep(1)
        failed = FindWindow(None, 'RunDLL')
        succeed = FindWindow(None, 'dll test')
        if failed + succeed == 0:
            sleep(1)
            failed = FindWindow(None, 'RunDLL')
            succeed = FindWindow(None, 'dll test')
        if failed + succeed == 0:
            result = -1
        if failed > 0:
            failedpid = findpid(None, 'RunDLL')
            kill(failedpid)
            print(asctime(),'unable to run dll')
            result = 0
        if succeed  > 0:
            succeedpid = findpid(None, 'dll test')
            kill(succeedpid)
            print(asctime(),'able to run dll')
            result = 1
        self.SignedDLLResult = result
       
    def RunUnsignedDLL(self):
        print ('running unsigned dll launch')
        Popen('rundll32.exe %s test' %pathToUnSignedDLL,shell = True)
        sleep(1)
        failed = FindWindow(None, 'RunDLL')
        succeed = FindWindow(None, 'dll test')
        if failed + succeed == 0:
            sleep(1)
            failed = FindWindow(None, 'RunDLL')
            succeed = FindWindow(None, 'dll test')
        if failed + succeed == 0:
            result = -1
        if failed > 0:
            failedpid = findpid(None, 'RunDLL')
            kill(failedpid)
            print(asctime(),'unable to run dll')
            result = 0
        if succeed  > 0:
            succeedpid = findpid(None, 'dll test')
            kill(succeedpid)
            print(asctime(),'able to run dll')
            result = 1
        
        self.UnsignedDLLResult = result
      
    def RunWriteAttempt(self):
        print ('running write attept')
        if os.path.exists('C:\\test'):
            pass
        else:
            os.mkdir('C:\\test')
        try:
            f = open('C:\\test\\test.txt', 'w')
            result = 1
            print (asctime(), 'ABLE TO WRITE')
            f.close()
            os.remove('C:\\test\\test.txt')
        except:
            print (asctime(), 'unable to write')
            result = 0
        self.WriteResult = result
        
    def RunReadAttempt(self):
        print ('running read attept')
        nRet = 0
        if nRet == 0:
            try:
                f = open(myPrivacyLocation, 'r')
                result = 1
                print (asctime(), 'ABLE TO READ')
                f.close()
            except PermissionError:
                result = 0
                print (asctime(), 'unable to read')
            except FileNotFoundError:
                result = -1
                print ('file not exist. Make sure it exists')
        self.ReadResult = result
        
    def RunScriptAttempt(self):
        print ('running script launch')
        runScript = Popen(pathToScriptLaunch,shell = True)
        sleep(myDelay)
        if runScript.poll() == 0:
            print ('script was able to launch')
            result = 1
        else:
            if (FindWindow(None, 'Windows Script Host') != 0):
                failedpid = findpid(None, 'Windows Script Host')
                kill(failedpid)
                result = 0
                print ('script was unable to launch')
            else:
                print ('other error in script launch. make sure script is vbs')
                result = -1
            
        self.ScriptResult = result
    def RunMemoryTest(self):
        print('running memory Test')
        nRet = 0
        procs = MyRunningProcesses()
        procs.getprocs()
        try:
                MyProcPid = procs.processlist[TestMemoryProc]
        except KeyError:
                print ('%s is not running. check for case sensitive' %TestMemoryProc)
                nRet = 1
        sleep(.1)
        if nRet == 0:
            MemRet = memtest(MyProcPid, TestMemoryProc)
            if type(MemRet[0]) == int:
                self.MemoryReadResult = MemRet[0]
            else:
                print ('something went wrong with mem read', MemRet[0])
            if type(MemRet[1]) == int:
                self.MemoryWriteResult = MemRet[1]
            else:
                print ('something went wrong with mem write', MemRet[0])

class SetExpectedResults():
    def __init__(self, SecLevel):
        self.SignedDLLResult = None
        self.UnsignedDLLResult = None
        self.WriteResult = None
        self.ReadResult = None
        self.ScriptResult = None
        self.MemoryReadResult = None
        self.MemoryWriteResult = None
        self.setExpected(SecLevel)
    def setExpected(self, Seclevel):
        if Seclevel == 0: #lockdown
            self.SignedDLLResult = 0
            self.UnsignedDLLResult = 0
            self.WriteResult = 0
            self.ReadResult = 0
            self.ScriptResult = 0
            self.MemoryReadResult = 0
            self.MemoryWriteResult = 0
        elif Seclevel == 1: #med
            self.SignedDLLResult = 1
            self.UnsignedDLLResult = 0
            self.WriteResult = 0
            self.ReadResult = 0
            self.ScriptResult = 0
            self.MemoryReadResult = 0
            self.MemoryWriteResult = 0
        elif Seclevel == 2: #install
            self.SignedDLLResult = 1
            self.UnsignedDLLResult = 1
            self.WriteResult = 0
            self.ReadResult = 0
            self.ScriptResult = 1
            self.MemoryReadResult = 1
            self.MemoryWriteResult = 1        
            
if __name__ == "__main__":
    #check if process directories and current directory are in userspace
    if os.environ['USERPROFILE'].lower() in os.getcwd().lower():
        nRet = 0
    else:
        nRet = 1
    procs = MyRunningProcesses()
    procs.getprocs()
    try:
        MyProcPid = procs.processlist[TestMemoryProc]
    except KeyError:
        print ('%s is not running. check for case sensitive' %TestMemoryProc)
        nRet = 1
    
    if nRet == 0:
        ##define AppGuardGui for testing
        APG = AppGuardGui()

        #add Python as guarded application
        APG.DeleteGuardedApp('python.exe')
        APG.DeleteGuardedApp('pythonw.exe')
        APG.AddGuardedApp('c:\\Python34\\python.exe', privacy = True)
        sleep(myDelay)
        APG.AddGuardedApp('c:\\Python34\\pythonw.exe', privacy = True)
        sleep(myDelay)
        
        for i in range (3):
            APG.SetSecurityLevel(i)
            sleep(myDelay)
            if i == 0:
                log ('testing lockdown')
            elif i == 1:
                log ('testing medium')
            elif i == 2:
                log ('testing install')
                
            Test = AppGuardTest()
            Expect = SetExpectedResults(i)
            Test.RunSignedDll()
            sleep(myDelay)
            Test.RunReadAttempt()
            sleep(myDelay)
            Test.RunScriptAttempt()
            sleep(myDelay)
            Test.RunWriteAttempt()
            sleep(myDelay)
            Test.RunUnsignedDLL()
            sleep(myDelay)
            Test.RunMemoryTest()
            sleep(myDelay)
            if Test.SignedDLLResult == Expect.SignedDLLResult:
                log('success. signed Dll launch test passed on sec level %d'%i)
            else:
                log('Failed. signed Dll launch test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.SignedDLLResult,Expect.SignedDLLResult))
            if Test.UnsignedDLLResult == Expect.UnsignedDLLResult:
                log('success. unsigned Dll launch test passed on sec level %d'%i)
            else:
                log('Failed. unsigned Dll launch test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.UnsignedDLLResult,Expect.UnsignedDLLResult))
            if Test.WriteResult == Expect.WriteResult:
                log('success. Write test passed on sec level %d'%i)
            else:
                log('Failed. Write test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.WriteResult,Expect.WriteResult))
            if Test.ReadResult == Expect.ReadResult:
                log('success. Read test passed on sec level %d'%i)
            else:
                log('Failed. Read test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.ReadResult,Expect.ReadResult))
            if Test.ScriptResult == Expect.ScriptResult:
                log('success. ScriptResult test passed on sec level %d'%i)
            else:
                log('Failed. ScriptResult test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.ScriptResult,Expect.ScriptResult))
            if Test.MemoryReadResult == Expect.MemoryReadResult:
                log('success. MemoryRead test passed on sec level %d'%i)
            else:
                log('Failed. MemoryRead test failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.MemoryReadResult,Expect.MemoryReadResult))
            if Test.MemoryWriteResult == Expect.MemoryWriteResult:
                log('success. MemoryWrite test passed on sec level %d'%i)
            else:
                log('Failed. MemoryWritetest failed on sec level %d'%i)
                log('Actual: %d, expected %d' %(Test.MemoryWriteResult,Expect.MemoryWriteResult))
            

        #now remove guarded applications
        APG.SetSecurityLevel(2)
        sleep(myDelay)
        APG.DeleteGuardedApp('python.exe')
        APG.DeleteGuardedApp('pythonw.exe')
