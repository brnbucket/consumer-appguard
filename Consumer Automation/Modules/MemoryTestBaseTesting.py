'''
Usage:

Dependancies:
python 3.4 installed on computer
pywin32 installed
'''
from Constants import *
from Logger import *
from ProcProcessor import *

#for appguard testing, make sure that notepad in system 32 is a guarded application


from ctypes import *
from ctypes.wintypes import *
from subprocess import Popen
from time import sleep, asctime
import os
import sys
import platform
import struct
import random
import string
logevents = True
sleepdelay = 0
initLogState = 'a' ##usually 'w'


TestProcesses = ("iexplore.exe",
                 "fileOpener.exe",
                 'notepad.exe',
                 #'syswowNotepad.exe',
                 #"EXCEL.EXE",
                 #"POWERPNT.EXE",
                 #'firefox.exe',
                 #'chrome.exe'
                 )
LoopRestTime = 0 

class memorytest():
    def __init__(self, pid,process):
        self.pid = pid
        self.process = process
        self.hProcHnd = None
        self.pBuffer = None
        self.readmem = None
        self.writemem = None
        self.addr = None
        self.getprochandle()
        sleep(sleepdelay)
        self.getsys()
        self.mbi = MEMORY_BASIC_INFORMATION()
    def getprochandle(self):
        self.hProcHnd = OpenProcess(PROCESS_ALL_ACCESS,
                                    False,
                                    self.pid)
        if self.hProcHnd == 0:
             if GetLastError()== 5:
                  self.hprocreturn= 'Access Denied'
             else:
                  self.hprocreturn = GetLastError()

        #allocate virtual 
        self.target_buff = create_string_buffer(4096)
        self.copied = create_string_buffer(64)
        self.p_copied = addressof(self.copied)
        try:
            self.pBuffer = VirtualAllocEx(self.hProcHnd, 0, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)
            sleep(sleepdelay)
        except:
            print(" failed allocate :"  + GetLastError())
            pass

        #try one more time to allocate virtual if failed
        if self.pBuffer == 0:
            self.pBuffer = VirtualAllocEx(self.hProcHnd, 0, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)
            sleep(sleepdelay)
           
       
    def getsys(self):
        #get system memory basic info and system info for address spaces
        
        self.sysinf = SYSTEM_INFO()
        GetSystemInfo(byref(self.sysinf))
        self.minappddr = self.sysinf.lpMinimumApplicationAddress
        self.maxappaddr = self.sysinf.lpMaximumApplicationAddress
       
    def memoryread(self):
        if (self.hProcHnd != 0):
            if logevents == True:
                print('====>Testing memory read<========')
            else:
                print ('====>Testing memory read<========')
            #querry minimum application address for BaseAddress
            VirtualQueryEx(self.hProcHnd,
                           self.minappddr,
                           addressof(self.mbi),
                           sizeof(self.mbi))
            self.addr = self.mbi.BaseAddress

            #create a pointer to capture the read
            pnt = pointer(self.target_buff)
            print ('reading memory of %s' %self.process)
            #read memory at BaseAddress
            readmem = ReadProcessMemory(self.hProcHnd,
                                        self.addr,
                                        pnt,
                                        4096,
                                        self.p_copied)
            print('memory read of %s result: %d'%(self.process,readmem))
            #log event if read
            if readmem == 1:
                if logevents == True:
                    print(' memory was read at ' + str(self.addr) + ' = ' + str(self.target_buff.value))
                else:
                    print(' memory was read at ' + str(self.addr) + ' = ' + str(self.target_buff.value))
            
            self.readmem = readmem
            print (str(self.process) + " memory read results: " + str(self.readmem))   
    def memorywrite(self):
        if (self.hProcHnd != 0):
            if logevents == True:
                print('====>Testing memory write<=======')
            else:
                print( '====>Testing memory write<=======')
            #try:

            #create random writedata string to write to process memory address space
            sizeofstring = 15
            writedata = ''.join(random.choice(string.ascii_letters+string.digits) for x in range(sizeofstring))

            '''

            #convert writedata to c_char_p
            ### done for python 2.7
            cdata = c_char_p(writedata[count.value:])
            '''
            count = c_ulong(0)
            #get writeadata length
            writelength = len(writedata)
            

            #if address not defined yet, you need to define it
            #do not need to do this if read has run
            if not self.addr:
                #querry minimum application address for BaseAddress
                VirtualQueryEx(self.hProcHnd,
                               self.minappddr,
                               addressof(self.mbi),
                               sizeof(self.mbi))
                self.addr = self.mbi.BaseAddress
            print ('writing memory of %s' %self.process)
            #write writedata to process memory address space
            writemem = WriteProcessMemory(self.hProcHnd,
                                          self.addr,
                                          writedata, #cdata for 2.7,
                                          writelength,
                                          byref(count))
            sleep(sleepdelay)
            print('memory write of %s result: %d'%(self.process,writemem))
           

            #see if writemem actually wrote to process address
            if writemem == 1:
                #read 
                if not self.readmem:
                    print('to verify if memory write worked, memory read should be tested first')
                    print('you may not be able to verify memory write')
                    cont = 1
                else:
                    if self.readmem == 0:
                        print('to verify if memory write worked, memory read needs to work')
                        print('cant verify memory write')
                        cont = 0
                    else:
                        if logevents == True:
                            print('memory was written, now reading to see what was written')
                            cont = 1
                if cont == 1:
                    #create a pointer to capture the read
                    pnt = pointer(self.target_buff)

                    
                    #read memory at that pointer
                    readmem = ReadProcessMemory(self.hProcHnd,
                                                self.addr, 
                                                pnt,                                                 4096,
                                                self.p_copied)
                
                    
                    #log event if read
                    if readmem == 1:
                        if self.target_buff.value == '':
                            if logevents == True:
                                print('memory read successfull, but nothing to be read at address space')
                        else:
                            if logevents == True:
                                print(' memory was read at ' + str(self.addr) + ' = ' + str(self.target_buff.value)) 
                
                              
            #log write results
            self.writemem = writemem
            if logevents == True:
                print (str(self.process) + " memory write results: " + str(self.writemem))
          
    #free memory and close handles         
    def closehandles(self):
        try:
            windll.kernel32.VirtualFreeEx(self.hProcHnd, self.pBuffer, 0, MEM_RELEASE)
        except:
            print( GetLastError())
        VirtualFreeEx(self.hProcHnd, MEM_RELEASE, sizeof(create_string_buffer(4096)),MEM_RELEASE)
        CloseHandle(self.hProcHnd)
        
        
    
def memtest(pid, proc = 'unspecified process'):
    '''
    tests the memory read and write of a process with the given pid
    example: memtest(2123)
    for logging purposes you may define process
    example: memtest (2123, 'BGClientGUI.exe')
    '''
    #checks against no specific process running
    if pid == 0:
        "no process"
        if logevents == True:
            print (proc + "does not exist")
    else:
        process = memorytest(pid, proc)

    
    if process.hProcHnd == 0:
        #define process results if python blocked from Opening process
        process.readmem = 'OpenProcess return 0.  Error: ' + str(process.hprocreturn)
        process.writemem = 'OpenProcess return 0.  Error: ' + str(process.hprocreturn)
        process.closehandles()
    else:
        #run process memory read
        process.memoryread()
        sleep(sleepdelay)
        #run process memory write
        process.memorywrite()
        sleep(sleepdelay)
        #free virtual memory and close process handles
        process.closehandles()
        sleep(sleepdelay)
    #return process write/read results
    return process.readmem, process.writemem


def test(logevents = False, proclist = TestProcesses):
    #get all running processes
    procs = MyRunningProcesses()
    procs.getprocs()
    results = -1
    
    if logevents == True:
        print('TESTING BG Process memory', mode = initLogState)
    fullresults = []
    #your list of testprocesses can be a list or tuple
    if type(TestProcesses) == list or type(TestProcesses) == tuple:
        for process in TestProcesses:
            #get process pid
            try:
                #get process pids
                processpids = procs.processlist[process.lower()]

                #run memory testing for each pid in process
                if type(processpids) == int:
                    pid = int(processpids)
                    print( 'Beginning memory testing for proc %s at pid %d' %(process, pid))
                    results = memtest(pid,process)
                    fullresults.append(results)
                else:
                    for pid in processpids:
                        print('Beginning memory testing for proc %s at pid %d' %(process, pid))
                        results = memtest(pid,process)
                        fullresults.append(results)
            except KeyError as x:
                print( 'process %s not running' %x)

    #if you have one test process in your test process list
    else:
        try:
            #get process pids
            processpids = procs.processlist[TestProcesses.lower()]

            #run memory testing for each pid in process
            if type(processpids) == int:
                pid = int(processpids)
                print('Beginning memory testing for proc %s at pid %d'%(TestProcesses, pid))

                results = memtest(pid,TestProcesses)
                fullresults.append(results)
            else:
                for pid in processpids:
                    print( 'Beginning memory testing for proc %s at pid %d' %(TestProcesses, pid))
                    results =memtest(pid,TestProcesses)
                    fullresults.append(results)
        except KeyError as x:
            print( 'process %s not running' %x)

    
    return fullresults
      

if __name__=='__main__':
     test(logevents = True)
     


