from tkinter import *
from time import sleep,asctime

class MessageMonitor(Frame):
    def __init__(self, master=None):
        Frame.__init__(self)
        self.master.title('MyLogger')
        self.master.rowconfigure(0, weight = 1)
        self.master.columnconfigure(0, weight = 1)
        self.grid (sticky = W+E+N+S)
        global listb
        listb = Listbox(self.master, width = 65, height = 15)
        listb.grid( row = 2, column = 0, columnspan = 4, sticky = W+E+N+S)
        yscroll = Scrollbar(self.master, command=listb.yview)
        yscroll.grid(row=2, column=1, rowspan = 3 , sticky='n'+'s')
        listb.configure(yscrollcommand=yscroll.set)
        listb.itemconfig
        

def SendMessageToMonitor(message, color):
    #listb.selection_clear(0)
    listb.insert(END, str(asctime()) + ': ' + message)
    listb.itemconfig(END, fg = color)
    #listb.selection_set(0)

if __name__=='__main__':
    MessageMonitor().mainloop()

