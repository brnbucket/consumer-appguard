from time import asctime




def log(message, Path = 'OutputLog.log', mode = 'a'):
    print (message)
    f = open(Path, mode=mode)
    f.write('%s: %s \n' %(str(asctime()),message))
    f.close()
            
    
