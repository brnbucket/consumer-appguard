#WRITTEN BY NATHAN HOISINGTON 4/28/2011
#Last Modified 4/30/2014
#BlueRidge Networks
'''
The purpose of sysstr.py is to automate systray actions
main testing function will be SystrayAction(ButtonName, Item, Subitem)
ButtonName is the name of the Systray button(user can find by mouseover)
Item is the item number on a popup window displayed by r. clicking the systray button
Subitem is the subitem number of the Item

examples:
    #to click the second button(refresh policy) of Blue Ridge AppGuard, which is 2nd from top
    #item has no subitems so 0 is used
    SystrayAction('Blue Ridge AppGuard', 2, 0)

    #to click on the 4th button(log serverity) of Blue Ridge AppGuard, which is 4th from top
    #select the 2nd subitem(Level 2), which is the 2nd subitem from the top
    SystrayAction('Blue Ridge AppGuard', 4, 2)

'''

import struct
from Constants import *
from time import sleep
from Gui import *



#gets the pid of a process with a given window handle
def getwindowpid(windowhandle):
    '''gets pid of window with given windowhandle'''
    pid = create_string_buffer(4)
    p_pid = addressof(pid)
    GetWindowThreadProcessId(windowhandle, p_pid)
    return struct.unpack("i",pid)[0]

class MySystrayHandler():
    def __init__(self):
        self.SystrayHandle = 0
        self.SystrayPid = 0
        self.SystryProcessHandle = 0
        self.SystrayButtonCount = 0
        self.ButtonLocation = 0
        self.cont = 0
        self.Buttons = {}
        self.StartingFunctions()
    def StartingFunctions(self):
        self.ButtonRects = {}
        self.findSystray()
        self.findButtons()
        self.findButtonRects()
    def findSystray(self):
        Shell_TrayWnd = FindWindow('Shell_TrayWnd', None)
        TrayNotifyWnd = FindWindowEx(Shell_TrayWnd, None, 'TrayNotifyWnd', None)
        SysPager = FindWindowEx(TrayNotifyWnd, None, 'SysPager', None)
        Systray = FindWindowEx(SysPager, None, 'ToolbarWindow32', None)
        self.SystrayHandle = Systray
        self.SystrayPid = getwindowpid(Systray)
        self.SystrayProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, False, self.SystrayPid)
        self.SystrayButtonCount = SendMessage(Systray, TB_BUTTONCOUNT,0,0)
    def findButtons(self):
        #allocate memory to pLVI buffer(retrieves sendmessage and reads momory of sent message)
        self.pLVI = VirtualAllocEx(self.SystrayProcessHandle, 0, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)
        for button in range(self.SystrayButtonCount):
            #Create string buffers to pass information into during sendMessage
            target_buff = create_string_buffer(4096)
            copied = create_string_buffer(4)
            p_copied = addressof(copied)

            #toolbar structure
            tbbutton = TBBUTTON()

            #Get Button information
            SendMessage(self.SystrayHandle,TB_GETBUTTON, button, self.pLVI)
            ReadProcessMemory(self.SystrayProcessHandle, self.pLVI, addressof(tbbutton), sizeof(tbbutton), p_copied)

            #Get Button Text from button id rescieved
            SendMessage(self.SystrayHandle,TB_GETBUTTONTEXT,tbbutton.idCommand,self.pLVI)
            ReadProcessMemory(self.SystrayProcessHandle, self.pLVI, addressof(target_buff), 4096, p_copied)
            self.Buttons[button]=(target_buff.value.decode('utf-8'))
    def findButtonRects(self):
        if len(self.Buttons) == 0:
            print ('no buttons')
            print ('make sure to call find Buttons command first')
            self.cont = 1

        #get location
        if self.cont == 0:
            for button in self.Buttons:
                pos = RECT()
                copied = create_string_buffer(4)
                p_copied = addressof(copied)
                SendMessage(self.SystrayHandle, TB_GETITEMRECT, button, self.pLVI )
                ReadProcessMemory(self.SystrayProcessHandle, self.pLVI, addressof(pos), sizeof(pos), p_copied)
                middley = (pos.top + pos.bottom)/2
                middlex = (pos.right + pos.left)/2
                #print ('xvalue = %d, yvalue = %d' %(middlex,middley))
                place = MAKELONG(middlex,middley)
                self.ButtonRects[button] = place

    def LocateButton(self,ButtonName):
        for i in self.Buttons:
            if ButtonName.lower() in str(self.Buttons[i]).lower():
                self.ButtonLocation = self.ButtonRects[i]

    def GetPopupWindow(self, popupexist = 0):
        #try for 10 seconds to get popup window
        self.PopupWindow = FindWindow('#32768', None)
        i = 100
        while self.PopupWindow == 0:
            sleep(.1)
            self.PopupWindow = FindWindow('#32768', None)
            
        if self.PopupWindow == 0:
            print ('could not find popup window')
            print ('make sure you click/rightclick action before running this command')
            self.cont = 1

        if self.PopupWindow == popupexist:
            print ('could not find new popup window')
            print ('make sure you click/righclick correct action/item before running this command')
            self.cont = 1
            
        if self.cont == 0:
            self.HmenuHandle = SendMessage(self.PopupWindow, MN_GETHMENU, 0,0)
            if self.HmenuHandle == 0:
                print ('window is not popup menu window')
                self.cont = 1

    def selectAction(self, action, submenu =None):
        if self.cont == 0:
            self.GetPopupWindow()
        if self.cont ==0:
            itemnumber,item = MenuAction(self.PopupWindow,
                                         action,
                                         Advanced = False,
                                         HmenuHandle = self.HmenuHandle,
                                         Click = False)
            #select the correct item on the list
            SendMessage(self.PopupWindow,WM_KEYDOWN,VK_DOWN,1)
            for i in range(itemnumber):
                SendMessage(self.PopupWindow,WM_KEYDOWN,VK_DOWN,1)

            #now bring up new popup menu
            SendMessage(self.PopupWindow,WM_KEYDOWN,VK_RIGHT,1)
            if submenu == None:
                SendMessage(self.PopupWindow,WM_KEYDOWN,VK_RETURN,0)
            
            else:
                sleep(.3)
                #renew popupwindow info
                self.GetPopupWindow(popupexist = self.PopupWindow)
                
                #now select proper item
                SendMessage(self.PopupWindow,WM_KEYDOWN,VK_DOWN,1)
                for i in range(submenu):
                    SendMessage(self.PopupWindow,WM_KEYDOWN,VK_DOWN,1)

                #finish
                SendMessage(self.PopupWindow,WM_KEYDOWN,VK_RETURN,0)
            
    def closeHandles(self):
        VirtualFreeEx(self.SystrayProcessHandle, self.pLVI, 0, MEM_RELEASE)
        CloseHandle(self.SystrayProcessHandle)

if __name__ == '__main__':
    ###appguard example set protection level to Medium
    self = MySystrayHandler()
    AppGuardIconStrings = 'appguard', 'Protection level', 'prevented'
    for string in AppGuardIconStrings:
        self.LocateButton(string)
        if self.ButtonLocation != 0:
            break

    if self.ButtonLocation != 0:
        RightClickButtonHold(self.SystrayHandle,self.ButtonLocation)
        #RightClickButtonHold(self.SystrayHandle,self.ButtonLocation)
        self.selectAction('Protection Level',
                          submenu = 1, # 0=lockdown,1=medium,2=install,3=off
                          )                
    else:
        print ('could not locate button')
    ###End appguard example set protection level to medium

