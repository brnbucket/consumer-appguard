
import threading
from time import sleep, asctime
MyTimeout = 20

'''
MyThread Status
status = -2 timeout error
status = -1 thread not run yet
status = 0 thread is running
status = 1 thread has successfully completed
'''

class MyThread():
    def __init__ (self, myfunc = None, args = None):
        self.status = -1
        self.myfunc = myfunc
        self.name = myfunc.__name__
        self.args = args
    def run(self, timeout = None):
        nRet = 0
        ##set up thread
        #no argument
        self.status = 0
        if self.args == None: 
            self.thread = threading.Thread(name = self.name, target = self.myfunc)
        #single argument
        elif type(self.args) == str or type(self.args) == int: 
            self.thread = threading.Thread(name = self.name, target = self.myfunc, args = (self.args,))
        #multi arguments
        else: 
            self.thread = threading.Thread(name = self.name, target = self.myfunc, args = (self.args))

        #start thread
                
        print('%s: starting %s' %(asctime(), self.name))
        self.thread.start()

        #monitor thread
        #run thread with timeout
        if timeout: 
            TimeThread = threading.Thread(name = 'sleep', target = sleep, args = (timeout,))
            TimeThread.start()

            while self.thread.isAlive():
                self.status = 0
                if TimeThread.isAlive() == False: #timeout
                    self.status = -2
                    print ('%s: function %s timed out' % (asctime(), self.name))
                    return self.status
        else:
            while self.thread.isAlive():
                self.status = 0

        if self.thread.isAlive() == False:
            self.status = 1
            print('%s: finished %s' %(asctime(), self.name))
        return self.status
        
    def stop(self): #_stop not working 3.4 due to unsave method
        self.thread._stop()
        
              
