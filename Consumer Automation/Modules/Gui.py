from Constants import *
from ProcProcessor import *
from Logger import *
from time import sleep
import win32gui
import re


class FindMyWindow():
    def __init__(self):
        self.Class = None
        self.WindowText = None
        self.WindowMatchList = {}
    def CallBackFunction(self,
                         hwnd,
                         args): #['classname','matchtext', 'click'])
        s = win32gui.GetClassName(hwnd)
        
        if s.startswith(args[0]):
            if args[1] == True:
                self.WindowMatchList[hwnd] = win32gui.GetWindowText(hwnd)
                if args[2] == True:
                    SendMessage(hwnd,BM_CLICK, True, 0)
            elif args[1] != 'ANY':
                #get window handles with specified names
                if win32gui.GetWindowText(hwnd).startswith(args[1]):
                    self.WindowMatchList[hwnd] = win32gui.GetWindowText(hwnd)
                    if args[2] == True:
                        SendMessage(hwnd,BM_CLICK, True, 0)
            elif args[1] == False:
                #get window handles with no names
                if win32gui.GetWindowText(hwnd) == '':
                    self.WindowMatchList[hwnd] = win32gui.GetWindowText(hwnd)
                    if args[2] == True:
                        SendMessage(hwnd,BM_CLICK, True, 0)
                    
    def ListWindows(self,
                    startWindow,
                    args):
        win32gui.EnumChildWindows(startWindow, self.CallBackFunction, args)

class FindMenuActions():
    def __init__(self):
        self.SubMenues = {}
        self.SubMenuItems = {}
        self.basicItems = {}
        self.itemRect = 0
        self.cont = 0
        self.MainMenu = 0
        
    def getMainMenu(self,Window):
        if Window == 0:
            print ('Window does not exist, cant continue')
            self.cont = 1

        #get MainMenu
        if self.cont == 0:
            self.MainMenu = GetMenu(Window)
            if self.MainMenu == 0:
                print ('menu not found')
                self.cont = 1
    def getSubMenues(self):
        if self.MainMenu == 0:
            print ('there is no main menu.  cant continue')
            self.cont == 0

        #Get submenus
        if self.cont == 0:
            #get menu item count
            MenuCount = GetMenuItemCount(self.MainMenu)
            for i in range(MenuCount):
                #create a string buffer to capture text of sub menue
                copied = create_string_buffer(256)
                p_copied = addressof(copied)
                #print ("getting name of item %d" %i)
                #now get submenu text
                GetMenuString(self.MainMenu, i, p_copied, sizeof(copied), MF_BYPOSITION)
                value = re.sub('\\x00', '', copied.raw.decode('utf -8'))
                #value = str(copied.raw).replace('\\x00', '')
                self.SubMenues[value.replace('&','')] = i
    def getSubMenuItems(self):
        if len(self.SubMenues) == 0:
            print ('no sub menues listed')
            self.cont = 1

        if self.cont == 0:
            for i in self.SubMenues:
                #Get select Menu
                SelectMenu = GetSubMenu(self.MainMenu,self.SubMenues[i])
                if SelectMenu == 0:
                    print ('no select menu')
                else:
                    SelectMenuCount = GetMenuItemCount(SelectMenu)

                itemList = []
                for item in range(SelectMenuCount):
                    #getSelectMenuText
                    copied = create_string_buffer(256)
                    p_copied = addressof(copied)
                    #print ('getting name of item %d in selected menu %s' %(item,i))
                    GetMenuString(SelectMenu, item, p_copied, 256, MF_BYPOSITION)
                    value = re.sub('\\x00', '', copied.raw.decode('utf -8'))
                    #value = str(copied.raw).replace('\\x00', '')
                    if value:
                        #print ( 'adding' )
                        #print ( "select item %d has value of %s" %(item, value.replace('&','')))
                        itemList.append(value.replace('&',''))
                    
                self.SubMenuItems[self.SubMenues[i]] = itemList
    def GetItemRect(self, Window, HmenuHandle, itemlist, itemText):
        if itemlist == {}:
            print ('no items in list to get location')
            self.cont = 1
        if self.cont == 0:
            for i in range(len(itemlist)):
                copied = create_string_buffer(256)
                p_copied = addressof(copied)
                #print ('getting name of item %d' %(i))
                GetMenuString(HmenuHandle, i, p_copied, sizeof(copied), MF_BYPOSITION)
                if itemText.lower() in itemlist[i].lower():
                    #print ('found')
                    pos = RECT()
                    GetMenuItemRect(Window,HmenuHandle,i,pos)
                    middley = (pos.top + pos.bottom)/2
                    middlex = (pos.right + pos.left)/2
                    place = MAKELONG(middlex,middley)
                    self.itemRect = place
                    break
            if self.itemRect == 0:
                print ('could not find location of item')
                self.cont = 1
                
                
    def GetItemsBasic(self, HmenuHandle):
        self.basicItems = {}
        SelectMenuCount = GetMenuItemCount(HmenuHandle)
        itemList = []
        for item in range(SelectMenuCount):
            #getSelectMenuText
            copied = create_string_buffer(256)
            p_copied = addressof(copied)
            #print ('getting name of item %d' %(item))
            GetMenuString(HmenuHandle, item, p_copied, sizeof(copied), MF_BYPOSITION)
            #print ("select item %d has value of %s" %(item, copied.value.replace('&','')))
            value = re.sub('\\x00', '', copied.raw.decode('utf -8'))
            #print (value)
            if value != '' :
                self.basicItems[item] = value.replace('&','')
            
        
def MenuAction (Window, #hwnd for advanced hmenu for not advanced
                action,
                Advanced = True,
                HmenuHandle = None,
                Click = True
                ):
    if Advanced == True:
        self = FindMenuActions()
        self.getMainMenu(Window)
        self.getSubMenues()
        self.getSubMenuItems()
    
        for i in self.SubMenuItems:
            for x in range(len(self.SubMenuItems[i])):
                if action.lower() in self.SubMenuItems[i][x].lower():
                    selectMenu = GetSubMenu(self.MainMenu, i)
                    selectItem = GetMenuItemID(selectMenu, x)
                    PostMessage(Window, WM_COMMAND, selectItem, 0)
    else: #get basic
        #window not used hmenuHandle used instead
        self = FindMenuActions()
        self.GetItemsBasic(HmenuHandle)
        #no submenu included, is just action id
    
        #see if action exists
        if action.lower() in str(self.basicItems).lower():
            self.cont = 0
        else:
            self.cont = 1

        if self.cont == 0:
            itemnumber = 0
            for item in self.basicItems:
                #print (action.lower())
                #print (self.basicItems[item].lower())
                if action.lower() in self.basicItems[item].lower():
                    #print 'found'
                    if Click == True:
                        selectItem = GetMenuItemID(HmenuHandle, item)
                        if selectItem <= 0:
                            print ('could not properly select item')
                            self.cont = 1
                            break
                        if self.cont == 0:
                            PostMessage(Window, WM_COMMAND, selectItem, 0)   
                    else:
                        return itemnumber, item
                else:
                    itemnumber +=1 
        else:
            print ('%s not found in %s' %(action.lower(), str(self.basicItems).lower()))
                
def RightClickButtonHold(WinHandle, ClickLocation):
    if ClickLocation == 0:
        print ('button location not found')
        print ('make sure you Locate Button first')
    else:
        SendMessage(WinHandle,WM_RBUTTONDOWN,0,ClickLocation)
        SendMessage(WinHandle,WM_RBUTTONDOWN,0,ClickLocation)

def RightClickButton(WinHandle, ClickLocation):
    if ClickLocation == 0:
        print ('button location not found')
        print ('make sure you Locate Button first')
    else:
        SendMessage(WinHandle,WM_RBUTTONDOWN,0,ClickLocation)
        SendMessage(WinHandle,WM_RBUTTONUP,0,ClickLocation)
        
def LeftClickButton(WinHandle, ClickLocation):
    if ClickLocation== 0:
        print ('button location not found')
        print ('make sure Locate Button first')
    else:
        SendMessage(WinHandle,WM_LBUTTONDOWN,0,ClickLocation)
        SendMessage(WinHandle,WM_LBUTTONUP,0,ClickLocation)
         
                
def ClickWindow(windowhandle, xcor,ycor):
    '''clicks window'''
    clickcoor = (ycor << 16) | xcor
    SendMessage(windowhandle,WM_LBUTTONDOWN,0,clickcoor)
    SendMessage(windowhandle,WM_LBUTTONUP,0,clickcoor)

def ClickButton(window,text):
    '''clicks button in window'''
    Button = FindWindowEx(window, None, 'Button', text)
    SendMessage(Button,BM_CLICK, True, 0)
def DBLClickButton(window,text):
    '''clicks button in window'''
    Button = FindWindowEx(window, None, 'Button', text)
    SendMessage(Button,BM_CLICK, True, 0)
    SendMessage(Button,BM_CLICK, True, 0)
    
def SetTrackBar(TrackBar, pos):
    '''sets Trackbar position'''
    #attempt to set position 5 times
    attempt = 0
    while (pos != SendMessage(TrackBar, TBM_GETPOS, 0 , 0)) and ( attempt < 5):
        SendMessage(TrackBar, TBM_SETPOS, True , pos)
        SendMessage(TrackBar, WM_SETFOCUS, 0, 0)
        SendMessage(TrackBar, WM_KEYUP, VK_UP, 1)
        sleep(.1)
        attempt +=1
    if attempt == 5:
        log('failed to set position of trackbar to %d' %pos)
    else:
        if (pos != SendMessage(TrackBar, TBM_GETPOS, 0 , 0)):
            log('failed to set position of trackbar to %d' %pos)
    

def GetListView(ListView, column_index= 0, FocusText = None):
    nRet = 0
    if nRet == 0:
        pid = create_string_buffer(4)
        p_pid = addressof(pid)
        GetWindowThreadProcessId(ListView, p_pid)
        procpid = struct.unpack("i",pid)[0]
        if p_pid == 0:
            nRet = 1
            log('could not find pid for listview window handle %d' %ListView)
            return nRet
    if nRet == 0:
        hProcHnd = OpenProcess(PROCESS_ALL_ACCESS, False, procpid)
        if hProcHnd == 0:
            nRet = 2
            log('could not open process handle for pid %d' %procpid)
    if nRet == 0:
        pLVI = VirtualAllocEx(hProcHnd, 0, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)
        pBuffer = VirtualAllocEx(hProcHnd, 0, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)
        if pLVI == 0 or pBuffer == 0:
            nRet = 3
            log('could not allocate memory for pid %d' %procpid)
            return nRet

    if nRet == 0:
        target_buff = create_string_buffer(4096)
        lvitem = LVITEM(8,3,column_index,3,-1,pBuffer,4096,0,0,0,0,0,0,0)
        copied = create_string_buffer(4)
        p_copied = addressof(copied)
        try:
            write = WriteProcessMemory(hProcHnd, pLVI, addressof(lvitem), sizeof(lvitem), p_copied)
        except:
            try:
                write = WriteProcessMemory(hProcHnd, pLVI, addressof(lvitem), sizeof(lvitem), p_copied)
            except:
                return GetLastError()
        if write == 0:
            nRet = 4
            log('could not write process memory of ListView items')
            VirtualFreeEx(hProcHnd, pBuffer, 0, MEM_RELEASE)
            VirtualFreeEx(hProcHnd, pLVI, 0, MEM_RELEASE)
            CloseHandle(hProcHnd)
            return nRet
    
    # iterate items in the ListView control
    if nRet == 0:
        itemcount = SendMessage(ListView, LVM_GETITEMCOUNT, 0, 0)
        if itemcount == 0:
            nRet = 5
            log('there are no items in listView')
            VirtualFreeEx(hProcHnd, pBuffer, 0, MEM_RELEASE)
            VirtualFreeEx(hProcHnd, pLVI, 0, MEM_RELEASE)
            CloseHandle(hProcHnd)
            return nRet
    if nRet == 0:
        item_texts = [] 
        for item_index in range(itemcount):
            SendMessage(ListView, LVM_GETITEMTEXTA, item_index, pLVI)
            target_buff = create_string_buffer(4096)
            try:
                read = ReadProcessMemory(hProcHnd, pBuffer, addressof(target_buff), 4096, p_copied)
            except:
                try:
                    read = ReadProcessMemory(hProcHnd, pBuffer, addressof(target_buff), 4096, p_copied)
                except:
                    return GetLastError()
            if read == 0:
                nRet = 6
                VirtualFreeEx(hProcHnd, pBuffer, 0, MEM_RELEASE)
                VirtualFreeEx(hProcHnd, pLVI, 0, MEM_RELEASE)
                CloseHandle(hProcHnd)
                log('could not read process memory of ListView items')
                return nRet
            if (FocusText != None):
                if FocusText.lower() in str(target_buff.value).lower():
                    
                    #alocate Mem
                    lvi = VirtualAllocEx(hProcHnd, None, 4096, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE)

                    #setup to select lvitem
                    lvitem = LVITEM()
                    lvitem.mask = LVIF_STATE
                    lvitem.state = LVIS_SELECTED | LVIS_FOCUSED
                    lvitem.stateMask = LVIS_SELECTED | LVIS_FOCUSED

                    #write to the location address of the lvitem
                    WriteProcessMemory(hProcHnd, lvi, addressof(lvitem), sizeof(lvitem), c_ulong(0))

                    #select item_index
                    SendMessage(ListView, LVM_SETITEMSTATE, item_index, lvi)
                    VirtualFreeEx(hProcHnd, lvi, 0, MEM_RELEASE)
                    VirtualFreeEx(hProcHnd, pBuffer, 0, MEM_RELEASE)
                    VirtualFreeEx(hProcHnd, pLVI, 0, MEM_RELEASE)
                    CloseHandle(hProcHnd) 
                    break
            item_texts.append(target_buff.value)
    VirtualFreeEx(hProcHnd, pBuffer, 0, MEM_RELEASE)
    VirtualFreeEx(hProcHnd, pLVI, 0, MEM_RELEASE)
    CloseHandle(hProcHnd) 
    if nRet == 0:
        return item_texts
    else:
        return nRet

#sets up value for a point to click
def MAKELONG(xcor, ycor): #(a,b)
    #print ('making lparam using xvalue = %d, and yvalue = %d' %(xcor,ycor))
    xcor = int(xcor)
    ycor = int(ycor)
    return (xcor & 0xffff) | ((ycor & 0xffff) << 16)


'''
#set trackbar example
APGWin = FindWindow(None,'AppGuard')
if APGWin =! 0:

    Trackbar = FindWindowEx(APGWin, None, TRACKBAR_CLASSA, None)
    SetTrackBar(Trackbar, 0) #0 is lockdown

else:
    print ('AppGuard window is not running')
'''

'''
#select export menu item example from bg client
BGClientWin = FindWindow(None, 'BorderGuard Client')
if BGClientWin !=0
    MenuAction(BGClientWin, 'import')
else:
    print ('BG Client window is not running')
'''
