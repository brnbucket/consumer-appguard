import os
import xml.dom.minidom
from xml.dom.minidom import parse

AppGuardPolicyXml = '%s\\blue ridge networks\\appguard\\appguardpolicy.xml' %os.environ['APPDATA']

class MyXMLParser():
    def __init__(self):
        self.xmlfile = None
        self.cont = 0
        self.XMLDOMTREE = None
        self.XMLdoc = None
    def OpenXML(self, xmlFile):
        self.xmlfile = xmlFile
        if os.path.exists(self.xmlfile) == False:
            print ('%s does not exist' %self.xmlfile)
            self.cont = 1
        if self.cont == 0:
            self.XMLDOMTREE = xml.dom.minidom.parse(xmlFile)
            self.XMLdoc = self.XMLDOMTREE.documentElement
    def GetXMLElements(self,
                       byTagName = False,#mention others
                       SearchLocation = None,
                       Search = None):
        if not self.XMLdoc:
            print ('need to OpenXML before running GetXMLElements')
            self.cont = 1
        if not SearchLocation:
            print ('need to give a Search Document')
        if not Search:
            print ('need to give Search value')
            self.cont = 1
        
        if self. cont == 0:
            if byTagName == True:
                CurrentElements = SearchLocation.getElementsByTagName(Search)
                return CurrentElements
                #CurrentElements[0].childNodes[0].data ==> gets data
                #CurrentElements[0].nextSibling ==> gets next item
        
    
