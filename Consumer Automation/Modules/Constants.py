from ctypes import *
from ctypes.wintypes import *
from win32com.client import Dispatch
import struct

#functions
MessageBox = windll.user32.MessageBoxW
FindWindow = windll.user32.FindWindowW
FindWindowEx = windll.user32.FindWindowExW
GetWindow = windll.user32.GetWindow
GetMenu = windll.user32.GetMenu
GetMenuItemCount = windll.user32.GetMenuItemCount
GetMenuItemInfo = windll.user32.GetMenuItemInfoW
SetMenuItemInfo = windll.user32.SetMenuItemInfoW
GetMenuInfo = windll.user32.GetMenuInfo
GetSubMenu = windll.user32.GetSubMenu
SendMessage = windll.user32.SendMessageW
PostMessage = windll.user32.PostMessageW
GetMenuItemID = windll.user32.GetMenuItemID
GetMenuString = windll.user32.GetMenuStringW
GetWindowText = windll.user32.GetWindowTextW
GetMenuItemRect = windll.user32.GetMenuItemRect
SendMessage = windll.user32.SendMessageW
SetParent = windll.user32.SetParent
DestroyWindow = windll.user32.DestroyWindow
GetWindowThreadProcessId = windll.user32.GetWindowThreadProcessId
VirtualAllocEx = windll.kernel32.VirtualAllocEx
VirtualQueryEx = windll.kernel32.VirtualQueryEx
OpenProcess = windll.kernel32.OpenProcess
ReadProcessMemory = windll.kernel32.ReadProcessMemory
WriteProcessMemory = windll.kernel32.WriteProcessMemory
VirtualFreeEx = windll.kernel32.VirtualFreeEx
GetSystemInfo = windll.kernel32.GetSystemInfo
CloseHandle = windll.kernel32.CloseHandle
FlushInstructionCache = windll.kernel32.FlushInstructionCache
GetLastError = windll.kernel32.GetLastError
CreateProcess = windll.kernel32.CreateProcessW

LVIF_STATE  = 8
LVIS_SELECTED = 2
LVIS_FOCUSED = 1


#contants
MF_BYPOSITION = 0x00000400
MB_OKCANCEL = 0x1
TBM_SETPOS = 1029
TRACKBAR_CLASSA = 'msctls_trackbar32'
WM_CLOSE = 0x0010
BM_CLICK = 245
WM_RBUTTONDOWN = 0x204

WM_USER = 1024
TBM_GETPOS = (WM_USER)
GW_CHILD = 5
GW_HWNDNEXT = 2
WM_SETTEXT = 12
WM_SETFOCUS = 0x0007
MN_GETHMENU = 481

TB_GETBUTTON = (WM_USER + 23)
TB_BUTTONCOUNT = (WM_USER + 24)
TB_GETITEMRECT = (WM_USER + 29)
TB_GETBUTTONTEXT = (WM_USER + 45)
LVM_FIRST = 4096 
LVM_GETITEMTEXTA = (LVM_FIRST + 45)
LVM_GETITEMCOUNT = (LVM_FIRST + 4)
LVM_GETITEMA = (LVM_FIRST + 5)
MEM_RESERVE = 8192
MEM_COMMIT = 4096
PAGE_READWRITE = 4
WM_LBUTTONDOWN = 513
WM_LBUTTONUP = 514
WM_KEYUP = 0x0101
WM_COMMAND = 273
LVM_SETITEMSTATE = (LVM_FIRST + 43)
LVM_GETITEMSTATE = (LVM_FIRST + 44)
LVIF_COLFMT = 0x00010000
LVIF_COLUMNS = 0x0200
PAGE_READONLY = 0x02
PROCESS_ALL_ACCESS = 0x1F0FFF

MEM_RESERVE = 8192
MEM_COMMIT = 4096
PAGE_READWRITE = 4
WM_LBUTTONDOWN = 513
WM_LBUTTONUP = 514
PROCESS_ALL_ACCESS = 0x1F0FFF
MEM_RELEASE = 32768
WM_GETTEXT = 0x000D
WM_KEYDOWN = 0x0100
VK_RBUTTON = 0x01
VK_LBUTTON = 0x02
VK_UP = 0x26
VK_RETURN = 0x0D
VK_RIGHT = 0x27
VK_DOWN = 0x28

NORMAL_PRIORITY_CLASS = 0x20
CREATE_PRESERVE_CODE_AUTHZ_LEVEL =  0x02000000



#Structures

class MEMORY_BASIC_INFORMATION(Structure):
     _fields_ =[("BaseAddress", c_void_p),#i think
                ("AllocationBase", c_void_p), #i think
                ("AllocationProtect", c_ulong), 
                ("RegionSize",c_size_t), #i think
                ("State", c_ulong),
                ("Protect", c_ulong),
                ("Type", c_ulong)]

                
class SYSTEM_INFO(Structure):
     _fields_ = [("wProcessorArchitecture", c_ushort),
                ("wReserved", c_ushort),
                ("dwPageSize", c_ulong),
                ("lpMinimumApplicationAddress", c_void_p),
                ("lpMaximumApplicationAddress", c_void_p),
                ("dwActiveProcessorMask", c_ulong), #i think
                ("dwNumberOfProcessors", c_ulong), 
                ("dwProcessorType", c_ulong),
                ("dwAllocationGranularity", c_ulong),
                ("wProcessorLevel", c_ushort),
                ("wProcessorRevision", c_ushort)]

class STARTUPINFO(Structure):
    _fields_ = [("cb", c_ulong),
                ("lpReserved", c_char_p), #i think
                ("lpDesktop", c_char_p), #i think
                ("lpTitle", c_char_p), #i think
                ("dwx", c_ulong),
                ("dwy", c_ulong),
                ("dwXSize", c_ulong),
                ("dwYSize", c_ulong),
                ("dwXCountChars", c_ulong),
                ("dwYCountChars", c_ulong),
                ("dwFillAttribute", c_ulong),
                ("dwFlags", c_ulong),
                ("wShowWindow", c_ushort),
                ("cbReserved2", c_ushort),
                ("lpReserved2", POINTER(c_byte)),#?
                ("hStdInput", c_void_p), 
                ("hStdOutput", c_void_p),
                ("hStdError", c_void_p)]
    
class PROCESSINFO(Structure):
    _fields_ = [("hProcess", c_void_p),
                ("hThread", c_void_p), 
                ("dwProcessId", c_ulong), 
                ("dwThreadId", c_ulong)]


class RECT(Structure):
    _fields_ = [("left", c_long),
                ("top", c_long), 
                ("right", c_long), 
                ("bottom", c_long)]

class TBBUTTON(Structure):
    _fields_ = [("iBitmap", c_int),
                ("idCommand", c_int), #maybe
                ("fsState", c_byte), #maybe
                ("fsStyle", c_byte),
                #("bReserved[6]", c_byte),64bit
                ("bReserved[2]", c_byte),
                ("dwData", c_int),
                ("iString", c_long)]

class LVITEM(Structure):
    _fields_ = [("mask", c_uint),
                ("iItem", c_int),
                ("iSubItem", c_int), 
                ("state", c_uint),
                ("stateMask", c_uint),
                ("pszText", c_char_p),##??
                ("cchTextMax", c_int),
                ("iImage", c_int),
                ("lParam", c_long),##??
                ("iIndent", c_int),#if (_WIN32_IE >= 0x0300)
                ("iGroupId", c_int), #if (_WIN32_WINNT >= 0x0501)
                ("cColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("puColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("piColFmt", c_int), #if (_WIN32_WINNT >= 0x0600)
                ("iGroup", c_int)] #if (_WIN32_WINNT >= 0x0600)

class LVITEM(Structure):
    _fields_ = [("mask", c_uint),
                ("iItem", c_int),
                ("iSubItem", c_int), 
                ("state", c_uint),
                ("stateMask", c_uint),
                ("pszText", c_char_p),##??
                ("cchTextMax", c_int),
                ("iImage", c_int),
                ("lParam", c_long),##??
                ("iIndent", c_int),#if (_WIN32_IE >= 0x0300)
                ("iGroupId", c_int), #if (_WIN32_WINNT >= 0x0501)
                ("cColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("puColumns", c_uint), #if (_WIN32_WINNT >= 0x0501)
                ("piColFmt", c_int), #if (_WIN32_WINNT >= 0x0600)
                ("iGroup", c_int)] #if (_WIN32_WINNT >= 0x0600)

class MENUINFO(Structure):
     _fields_ = [("cbSize", c_ulong),
                 ("fMask", c_ulong),
                 ("dwStyle", c_ulong),
                 ("cyMax", c_uint),
                 ("hbrBack", HANDLE), #HBRUSH
                 ("dwContextHelpID", c_ulong ), 
                 ("dwMenuData", c_ulong)] ###???ULONG_PTR

class MENUITEMINFO (Structure):
     _fields_ = [("cbSize", c_uint),
                 ("fMask", c_uint),
                 ("fType", c_uint),
                 ("fState", c_uint),
                 ("wID", c_uint), 
                 ("hSubMenu", HANDLE ), #Hmenu
                 ("hbmpChecked", HANDLE),#HBITMAP
                 ("hbmpUnchecked",HANDLE ),#HBITMAP
                 ("dwItemData", c_ulong), ##??ULONG_PTR
                 ("dwTypeData", c_char_p), #LPTSTR (use for LPSTR) 
                 ("cch",  c_uint),
                 ("hbmpItem",HANDLE )]#HBITMAP
                 
                 
     
  
  
 
  
  

 
