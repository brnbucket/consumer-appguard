# README #

Consumer appguard

## Consumer AppGuard Terminology (simple definitions) ##

* **system-space** --> directories used by operating system (ex:  "C:\", "C:\Windows")
* **user-space** --> diretories/drives used by User (ex: "C:\Users\MyUser", "F:\")
* **Guarded Execution** --> blocking a process from writing to **system-space**
* **Private Folder**--> a specified folder with restricted read and write privileges
* **Privacy Protection** --> blocking a process from reading from the **Private Folder**
* **Memory Protection** --> blocking a process from writing and/or reading the process memory of another process
* **Launch Protecton**--> AppGuard blocks a process from launching
* **Guarded Application**--> an application that Appguard protects against.  This application will have **Guarded Execution**, and optional **Privacy Protection** and **Memory Protection**
* **user-space Application**--> an application that was launched from **user-space**.  These application with have **Guarded Execution**, **Privacy Protection** and **Memory Protection**
* **signed** --> an application whose executable is signed
* **publisher** --> a **signed** application whose signature is known by Appguard
* **script**--> .bat, .cmd, .vbs, etc files
* **dll** --> dll calls.  May use this syntax to test "rundll32.exe <path_to_dll> <function_in_dll>"


## HIGH SECURITY Functional Tests##
###Guarded Execution Test 

* Verify **Guarded Application** cant write to **system-space**
* Verify **Guarded Application** can write to **user-space**
* Verify  **signed/publisher user-space Application** cant write to **system-space** 
* Verify  **signed/publisher user-space Application** can write to **user-space**


###Privacy Protection Test

* Verify **Guarded Application** with privacy protection cant read from **Private Folder**
* Verify **Guarded Application** without privacy protection can read from **Private Folder**
* Verify  **signed/publisher user-space Application** cant read from **Private Folder**

###Memory Protection Test
* Verify **Guarded Application** with memory read protection cant read memory on another process
* Verify **Guarded Application** with memory write protection cant write memory to another process
* Verify **Guarded Application** without memory read protection can read memory on another process
* Verify **Guarded Application** without memory write protection can write memory to another process
* Verify  **signed/publisher user-space Application** cant read or write memory of another process

###Launch Protection Test

* verify non-**signed** applications are blocked from launching from **user-space**
* verify **signed** applications are not blocked from launching from **user-space**
* verify all **script** applications are blocked from launching from **user-space**
* verify non-**signed dlls** are blocked from launching from **user-space**
* verify **signed dll** are not blocked from launching from **user-space**


### What is this repository for? ###

* Consumer appguard automation tests for all Consumer AppGuard Functional Test Cases listed above across all windows os
* Consumer appguard stress tests


###Current Script Limitations###

* Memory Guard Scripts on windows 8
* Windows 10 not tested


### How do I get set up? ###

* Install python 3.4
* Install win32com.client for python3.4